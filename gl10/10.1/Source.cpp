#include "../../include/std_lib_facilities.h"


double file_sum(string path) {
    int total = 0, n;

    ifstream ifs(path.c_str());
    
    if (!ifs) error("Could not open file.");

    while(ifs >> n) {
        total += n;
    }

    if (!cin.eof()) {
        cout << "Reading interrupted." << endl;
    }

    return total;
}

int main () {
    string path = "num.txt"; //num.txt must be in the same file as executable

    double sum = file_sum(path);

    cout << sum << endl;

    keep_window_open();

    return 0;
}