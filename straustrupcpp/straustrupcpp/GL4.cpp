// straustrup Part 4


#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>

double MinMax(double tempch1, double tempch2)
{
	if (tempch1 > tempch2)
	{
		return tempch2;
	}

	else
	{
		return tempch1;
	}
}




int main()
{
	double tempo1, tempo2;
	tempo2 = 0;
	std::string measure = "p";
	char kostl = 't';
	
	const double in_to_cm = 2.54;
	const double ft_to_cm = 30.84;
	const double m_to_cm = 100;
	const double cm_to_cm = 1;
	const double cm_to_m = 0.01;

	while (std::cin >> kostl >> tempo1 >> measure)
	{
		int i = 0;
		std::vector <double> vec;

		if (kostl == '|')//it causes a trouble with exiting the program because tempo1 is double
		{				  //not char
			double sum = 0;
			std::cout << "Sum= " << std::accumulate(vec.begin(), vec.end(), 0.0) << "\n" << "Size= " << vec.size();
		}

		
		
		if (measure == "m")
		{
			tempo1 *= m_to_cm;
			tempo2 *= m_to_cm;
		}
		else if (measure == "in")
		{
			tempo1 *= in_to_cm;
			tempo2 *= in_to_cm;
		}
		else if (measure == "ft")
		{
			tempo1 *= ft_to_cm;
			tempo2 *= ft_to_cm;
		}
		else if (measure == "cm")
		{
			tempo1 *= cm_to_cm;
			tempo2 *= cm_to_cm;
		}
		else
		{
			std::cout << "Something went wrong: check yourself" << "\n";
		}
		
		vec.push_back(tempo1);

		if (MinMax(tempo1, tempo2) == tempo1) //task 6
		{
			std::cout << "min= " << vec[i]*cm_to_m << "m" << "\n";
		}

		else
		{
			std::cout << "max= " << vec[i]*cm_to_m << "m" << "\n";
		}
		

		i++;
		tempo2 = tempo1;
	}
	return 0;
}






