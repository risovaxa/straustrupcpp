// straustrupcpp_gl5.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <exception>

class myexception : public std::exception
{
	virtual const char* what() const throw()
	{
		return "My exception happened";
	}
} myex;

int main()
{
	try {
		
		throw myex;
		system("Pause");
		return 0;
	}
	catch (std::exception& е) {
		std::cerr << "Mistake : " << e.what() << ' \n ';
		system("Pause");
		return 1;
	}
	catch (...) {
		std::cerr << "Unknown exception ! \n";
		system("Pause");
		return 2;
	}
}

